# Graphics Coursework Year 2 #

Assignment was to create a multiple scene animation for the advertisement of BMW Motors in C++
using OpenGL. 

Techniques used in code :

- Morphing
- transformations
  -rotation
  -scaling
  -translation
  -reflection
- Colour cycling
- Viewports (Multiple)
- Buffering
- 3D translation

Mark - 1st (88%)