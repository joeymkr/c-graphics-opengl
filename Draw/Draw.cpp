// Draw.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <Math.h>
#include <stdlib.h>
#include <stdio.h>

#include "glut.h"

//pointers to arrays containing car co-ordinates
int (*pointerToCurrentCar)[2];
int (*pointerToNextCar)[2];
int (*pointerToCurrentHeadlight)[2];
int (*pointerToNextHeadlight)[2];
int (*pointerToCurrentGrill)[2];
int (*pointerToNextGrill)[2];
int (*pointerToCurrentRadiator)[2];
int (*pointerToNextRadiator)[2];
int (*pointerToCurrentScreen)[2];
int (*pointerToNextScreen)[2];
int (*pointerToCurrentDoor)[2];
int (*pointerToNextDoor)[2];
int (*pointerToCurrentWheels)[2];
int (*pointerToNextWheels)[2];
//pointer array containing all car co-ordinates
int (*pointerToCars[14])[2];

//slogan
char *title = "Performance  &  Design";
//array for crankshaft and piston viewport
float sceneoneXY [2] = {0.0,0.0};
//frame counter
int frame = 0;
int morphCounter = 0;
//pause between car morphs
//car2view - indicates which car is in view
bool pause = false, car2View = false;
int pistonX = 0;
int pistonY = 0;
int pistonX2 = -100;
int pistonY2 = -100;
int conRodX = 260;
int conRodY = 400;
int theta = 0.0;
bool colour1 = true;
//variables for colour cycling
float red = 0.3, green = 0.4, blue = 0.5;
float carColour[2][3] = {{0.3,0.4,0.5},{0.4,0.0,0.0}};

float circleX;
float circleY;
float sX = 1.0;
float sY = 1.0;
float engScale = 1.0;
float engX = -500;
float proportion = 0.000;
float alpha2 = 0.0, alpha3 = 0.0, alpha4 = 0.0, carAlpha = 0.0, engineAlpha = 1.0, logoAlpha = 0.0;
//return variable for function tween
float * tweenReturn;
//co-ordinates for valves
int valveSource [8][2] = {{150,600},{225,600},{290,600},{350,600},{600,600},{600,600},{600,600},{600,600}};
int valveDest [8][2] = {{150,410},{225,390},{290,370},{350,355},{175,420},{245,395},{310,375},{365,360}};
//coordinates for engine cases - source and destination
int engineRightDest [22][2] = {{201,1},{201,380},{250,380},{450,250},{450,150},{470,120},{470,60},{450,40},{280,1},{201,1},{255,360},{255,170}
								,{280,165},{430,150},{255,170},{315,130},{315,35},{280,1},{460,125},{315,130},{315,35},{460,60}};

int engineRightSource [22][2] = {{501,1},{501,380},{550,380},{750,250},{750,150},{770,120},{770,60},{750,40},{580,1},{501,1},{555,360},{555,170}
								,{580,165},{930,150},{555,170},{615,130},{615,35},{580,1},{760,125},{615,130},{615,35},{760,60}};

int engineLeftDest [8][2] = {{201,1},{201,380},{151,380},{151,170},{97,130},{97,35},{131,1},{201,1}};

int engineLeftSource [8][2] = {{-101,1},{-101,380},{-251,380},{-251,170},{-297,130},{-297,35},{-231,1},{-101,1}};

//arrays for drawing to screen in function car
int carSource [46][2];
int headLightSource[16][2];
int grillSource[16][2];
int radiatorSource[21][2];
int screenSource[11][2];
int doorSource[7][2];
int wheelsSource[2][2];
//co-ordinates for cars
int car1[46][2] = {{15,53},{11,89},{12,121},{25,149,},{40,167},{56,176},{70,183},{90,190},{107,197},{125,206},
						{139,208},{157,230},{177,244},{191,257},{207,263},{226,273},{251,275},{274,279},{304,281},{332,281},
						{354,281},{379,279},{393,277},{409,275},{427,264},{445,252},{458,241},{472,223},{486,216},{500,200},
						{504,170},{503,143},{467,91},{354,55},{293,36},{261,31},{231,31},{206,33},{189,38},{156,38},
						{125,36},{104,40},{80,42},{65,35},{43,33},{15,53}};

int car2[46][2] = {{14,135},{19,175},{34,182},{41,219},{54,231},{113,249},{164,262},{212,267},{239,289},{284,322},
						{310,334},{332,342},{382,347},{456,351},{511,349},{567,347},{615,342},{660,333},{689,311},{714,278},
						{730,251},{734,219},{739,184},{740,154},{733,130},{724,117},{717,92},{634,90},{578,85},{542,83},
						{486,78},{448,76},{288,76},{256,69},{226,67},{204,69},{180,78},{143,79},{139,72},{116,70},
						{96,81},{77,79},{54,83},{38,92},{25,108},{14,135}};

int headlightpoints [16][2] = {{29,152},{46,147},{50,120},{16,127},{29,152},
								{205,143},{235,148},{250,150},{273,154},{288,152},{276,125},{259,119},{236,117},{214,117},{184,116},{205,143}};

int headlightpoints2 [16][2] = {{40,217},{62,217},{61,190},{34,192},{40,217},{187,215},{219,213},{261,211},
									{286,211},{291,199},{276,182},{243,181},{195,182},{164,181},{173,197},{187,215}};

int grillpoints [16][2] = {{66,144},{92,150},{109,146},{105,116},{86,114},{69,117},{64,132},{66,144},
{123,146},{148,148},{174,144},{164,119},{145,114},{121,116},{115,128},{123,146} };

int grillpoints2 [16][2] = {{65,210},{72,220},{98,220},{106,210},{103,186},{95,182},{68,186},{65,210},
							{110,206},{120,220},{155,220},{163,204},{155,186},{145,181},{111,182},{110,206}};

int radiatorpoints [21][2] = {{19,87},{49,87},{45,70},{19,74},{19,87}, //left radiator 5
							{60,85},{175,83},{181,67},{176,59},{126,59},{99,61},{63,63},{57,70},{60,85},//centre rad 9
							{206,85},{237,85},{249,81},{256,72},{259,59},{211,59},{206,85} };//right rad 7

int radiatorPoints2 [21][2] = {{27,126},{56,126},{56,108},{40,110},{29,110},
								{50,126},{80,126},{104,126},{135,121},{135,103},{113,103},{100,100},{80,105},{50,108},
								{135,121},{159,123},{184,114},{209,103},{190,96},{161,97},{135,103}};

int windscreenPoints[11][2] = {{205,264},{248,264},{289,267},{324,267},
								{360,258},{351,217},{338,206},{316,206},{257,210},{184,210},{138,208}};

int windscreenPoints2 [11][2] = {{218,266},{296,324},{316,334},{336,338},{365,340},{427,342},{485,342},{441,277},{422,266},{312,271},{218,266}};

int doorPoints [7][2] = {{367,199},{374,253},{390,266},{415,262},{432,213},{420,190},{440,195}};

int doorPoints2 [7][2] = {{456,260},{489,322},{518,342},{565,340},{579,260},{545,240},{575,240}};

int wheelsPos[2][2] = {{350,70},{250,50}};
int wheelsPos2 [2][2] = {{380,80},{350,20}};
float wheelsScale [2] = {0.5,0.8};

//draws the letter B
//only draws 1 half of the B
void letterB(){
	glColor4f(1.0,1.0,1.0,logoAlpha);

	glBegin(GL_POLYGON);
		float semiX = 0.0, semiY = 0.0;
		for(int i =0; i < 100;i++){
			float cirTheta = 3.1415926f * float(i) / float(100);//get the current angle 

			semiX = 30 * cosf(cirTheta);//calculate the x component 
			semiY = 30 * sinf(cirTheta);//calculate the y component 

			glVertex2f(semiX,semiY);//output vertex 
		}
	glEnd();
	glBegin(GL_POLYGON);
		glVertex2i(-30,1);
		glVertex2i(30,1);
		glVertex2i(30,-15);
		glVertex2i(-30,-15);
	glEnd();
	glBegin(GL_POLYGON);
	glColor4f(0.0,0.0,0.0,logoAlpha);
		for(int i =0; i < 100;i++){
			float cirTheta = 3.1415926f * float(i) / float(100);//get the current angle 

			semiX = 15 * cosf(cirTheta);//calculate the x component 
			semiY = 15 * sinf(cirTheta);//calculate the y component 

			glVertex2f(semiX,semiY);//output vertex 
		}
	glEnd();
}
//draws letter M
//only draws 1 half of the M
void letterM(){
	glColor4f(1.0,1.0,1.0,logoAlpha);
	glBegin(GL_POLYGON);
		glVertex2i(0,0);
		glVertex2i(0,60);
		glVertex2i(20,60);
		glVertex2i(20,0);
	glEnd();
	glBegin(GL_POLYGON);
		glVertex2i(20,60);
		glVertex2i(40,25);
		glVertex2i(40,5);
		glVertex2i(15,40);		
	glEnd();
		
}
//draws white badge
void badge(float r, float g, float b, int size){
	glBegin(GL_POLYGON);
		float semiX, semiY;
		glColor4f(r,g,b,logoAlpha);
		for(int i =0; i < 100;i++){
			float cirTheta = 2 * 3.1415926f * float(i) / float(100);//get the current angle 

			semiX = size * cosf(cirTheta);//calculate the x component 
			semiY = size * sinf(cirTheta);//calculate the y component 

			glVertex2f(semiX,semiY);//output vertex 
		}
	glEnd();
}
//draws 1 quarter of circle
void quaterCircle(float r, float g, float b, int size){
	glBegin(GL_POLYGON);
		float semiX, semiY;
		glColor4f(r,g,b,logoAlpha);
		for(int i =0; i < 100;i++){
			float cirTheta = 3.1415926f * float(i) / float(100);//get the current angle 

			semiX = size * cosf(cirTheta/2);//calculate the x component 
			semiY = size * sinf(cirTheta/2);//calculate the y component 

			glVertex2f(semiX,semiY);//output vertex 
		}
	glEnd();
	
	glBegin(GL_TRIANGLES);
		glVertex2i(1,0);
		glVertex2i(1,81);
		glVertex2i(81,0);
	glEnd();
}
//draws piston
void piston(float a)
{
	glColor4f (0.5,0.6,0.7, a);

	glBegin(GL_LINE_LOOP);
		glVertex2i(240, 400);
		glVertex2i(300, 400);
		glVertex2i(300, 350);
		glVertex2i(240, 350);
	glEnd();
}
//draws connection rod
void connectionRod(float a)
{
	glColor4f (0.5,0.6,0.7, a);

	glBegin(GL_LINE_LOOP);
		glVertex2i(260, 350);
		glVertex2i(280, 350);
		glVertex2i(conRodX+20, conRodY-150);
		glVertex2i(conRodX, conRodY-150);
	glEnd();
}
//draws crankshaft
void crankShaft(float a)
{
	float crankAlpha = 0.0;
	if (a > 0.0 && a < 0.5){ crankAlpha = (a + 0.5);}
	else if (a >= 0.5){crankAlpha = 1.0;}

	glColor4f (0,0,0,crankAlpha);

	glBegin(GL_POLYGON); 
	for(int ii = 0; ii < 100; ii++) 
	{ 
		float theta = 2.0f * 3.1415926f * float(ii) / float(100);//get the current angle 

		circleX = 58.5 * cosf(theta);//calculate the x component 
		circleY = 58.5 * sinf(theta);//calculate the y component 

		glVertex2f(circleX + 270,circleY + 195);//output vertex 

	}

	glEnd();

	glColor4f (0.5,0.6,0.7, a);

	glBegin(GL_LINE_LOOP); 
	for(int ii = 0; ii < 100; ii++) 
	{ 
		float theta = 2.0f * 3.1415926f * float(ii) / float(100);//get the current angle 

		circleX = 58.5 * cosf(theta);//calculate the x component 
		circleY = 58.5 * sinf(theta);//calculate the y component 

		glVertex2f(circleX + 270, circleY + 195);//output vertex 

	}
	glEnd();

	glBegin(GL_LINE_STRIP);
		glVertex2i(230, 153);
		glVertex2i(260, 170);
		glVertex2i(260, 220);
		glVertex2i(230, 237);
	glEnd();

}
//draws a valve
void valves(){
	glColor3f(0.5,0.6,0.7);

	glBegin(GL_LINE_STRIP);
		glVertex2i(0,0);
		glVertex2i(20,0);
		glVertex2i(20,5);
		glVertex2i(12,7);
		glVertex2i(12,50);
		glVertex2i(8,50);
		glVertex2i(8,7);
		glVertex2i(0,5);
		glVertex2i(0,0);
	glEnd();
}
//draws left engine case
void engineLeft(){
	glColor4f(0.0,0.0,0.0, engineAlpha);

	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i < 8; i++){
			glVertex2i(engineLeftSource[i][0], engineLeftSource[i][1]);
		}
	glEnd();

	glColor4f(0.5,0.6,0.7, engineAlpha);

	glBegin(GL_LINE_STRIP);
		for (int i = 0; i < 8; i++){
			glVertex2i(engineLeftSource[i][0], engineLeftSource[i][1]);
		}
	glEnd();
}
//draws right engine case
void engineRight(){

	int counter = 0;
	glColor4f(0.0,0.0,0.0, engineAlpha);

	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i < 10; i++){
			glVertex2i(engineRightSource[i][0], engineRightSource[i][1]);
		}
	glEnd();

	glColor4f(0.5,0.6,0.7, engineAlpha);
	 //10
	glBegin(GL_LINE_STRIP);
		for (int i = 0; i < 10; i++){
			glVertex2i(engineRightSource[counter][0], engineRightSource[counter][1]);
			counter++;
		}
	glEnd();
	//8
	glBegin(GL_LINES);
		for (int i = 0; i < 8; i++){
			glVertex2i(engineRightSource[counter][0], engineRightSource[counter][1]);
			counter++;
		}
	glEnd();
	//4
	glBegin(GL_LINE_STRIP);
		for (int i = 0; i < 4; i++){
			glVertex2i(engineRightSource[counter][0], engineRightSource[counter][1]);
			counter++;
		}
	glEnd();
}
//draws pistons and crankshafts. repeats functions piston, connectionrod and crankShaft to draws four of each
void sceneOne(){
	// Draw and Translate piston 4
		glPushMatrix();
			glTranslatef(pistonX+270, pistonY2+78, 0.0);
			glScalef((sX * 0.7), (sY * 0.7),0.0);
				piston(alpha4);
				connectionRod(alpha4);
		glPopMatrix();

		//Draw and Rotate camshaft 4
		glPushMatrix();
			glTranslatef(270,60, 0.0);
			glScalef((sX * 0.7), (sY * 0.7),0.0);
			glTranslatef(270,195, 0.0);
			glRotatef(theta, 0, 0, 1.0);
			glTranslatef(- 270,- 195, 0.0);
				crankShaft(alpha4);
		glPopMatrix();

		//Draw and Translate piston 3
		glPushMatrix();
			glTranslatef(pistonX+180, pistonY+46, 0.0);
			glScalef((sX * 0.8), (sY * 0.8),0.0);
				piston(alpha3);
				connectionRod(alpha3);
		glPopMatrix();

		//Draw and Rotate camshaft 3
		glPushMatrix();
			glTranslatef(180,40, 0.0);
			glScalef((sX * 0.8), (sY * 0.8),0.0);
			glTranslatef(270,195, 0.0);
			glRotatef(theta, 0, 0, 1.0);
			glTranslatef(- 270,- 195, 0.0);
				crankShaft(alpha3);
		glPopMatrix();

		//Draw and Translate piston 2
		glPushMatrix();
			glTranslatef(pistonX+90, pistonY2+20, 0.0);
			glScalef((sX * 0.9), (sY * 0.9),0.0);
				piston(alpha2);
				connectionRod(alpha2);
		glPopMatrix();

		//Draw and Rotate camshaft 2
		glPushMatrix();
			glTranslatef(90,20, 0.0);
			glScalef((sX * 0.9), (sY * 0.9),0.0);
			glTranslatef(270,195, 0.0);
			glRotatef(theta, 0, 0, 1.0);
			glTranslatef(- 270,- 195, 0.0);
				crankShaft(alpha2);
		glPopMatrix();
		

		//Draw and Translate piston 1
		glPushMatrix();
			glTranslatef(pistonX, pistonY, 0.0);
				piston(1.0);
				connectionRod(1.0);
		glPopMatrix();

		//Draw and Rotate camshaft 1
		glPushMatrix();
			glTranslatef(270,195, 0.0);
			glRotatef(theta, 0, 0, 1.0);
			glTranslatef(- 270,- 195, 0.0);
				crankShaft(1.0);
		glPopMatrix();
}
//repeats the draw function of valves to draw 8 valves
void drawValves(){
	glViewport(0,0, 1000,1000);

	glPushMatrix();
	glTranslatef(valveSource[3][0], valveSource[3][1], 0.0);
	glScalef((sX * 0.4),(sY * 0.4),0.0);
	glRotatef(-35.0,0,0,-1.0);
		valves();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(valveSource[2][0], valveSource[2][1], 0.0);
	glScalef((sX * 0.6),(sY * 0.6),0.0);
	glRotatef(-35.0,0,0,-1.0);
		valves();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(valveSource[1][0], valveSource[1][1], 0.0);
	glScalef((sX * 0.8),(sY * 0.8),0.0);
	glRotatef(-35.0,0,0,-1.0);
		valves();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(valveSource[0][0], valveSource[0][1], 0.0);
	glRotatef(-35.0,0,0,-1.0);
		valves();
	glPopMatrix();

	//second set of valves
	glPushMatrix();
	glTranslatef(valveSource[7][0], valveSource[7][1], 0.0);
	glScalef((sX * 0.4),(sY * 0.4),0.0);
	glRotatef(-35.0,0,0,1.0);
		valves();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(valveSource[6][0], valveSource[6][1], 0.0);
	glScalef((sX * 0.6),(sY * 0.6),0.0);
	glRotatef(-35.0,0,0,1.0);
		valves();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(valveSource[5][0], valveSource[5][1], 0.0);
	glScalef((sX * 0.8),(sY * 0.8),0.0);
	glRotatef(-35.0,0,0,1.0);
		valves();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(valveSource[4][0], valveSource[4][1], 0.0);
	glRotatef(-35.0,0,0,1.0);
		valves();
	glPopMatrix();
}
//draws all car points and car detail points (screen, radiator, grill etc)
void car(){
	glColor4f(red,green,blue,carAlpha);
	glBegin(GL_TRIANGLE_FAN);
		int counter = 0;
		for (int i = 0; i <46; i++){
			if(i == 30){glColor4f((red-0.3),(green-0.3),(blue-0.3),carAlpha);}
			glVertex2i(carSource[counter][0], carSource[counter][1]);
			counter++;
		}
	glEnd();
	glColor4f(1.0,1.0,1.0,carAlpha);
	//left head light
	counter = 0;
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <5; i++){
			glVertex2i(headLightSource[counter][0], headLightSource[counter][1]);
			counter++;
		}
	glEnd();
	//right head light
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <11; i++){
			glVertex2i(headLightSource[counter][0], headLightSource[counter][1]);
			counter++;
		}
	glEnd();
	//left grill
	glColor4f(0.1,0.1,0.1,carAlpha);
	counter = 0;
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <8; i++){
			glVertex2i(grillSource[counter][0], grillSource[counter][1]);
			counter++;
		}
	glEnd();
	//right grill
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <8; i++){
			glVertex2i(grillSource[counter][0], grillSource[counter][1]);
			counter++;
		}
	glEnd();
	//radiator
	counter = 0;
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <5; i++){
			glVertex2i(radiatorSource[counter][0], radiatorSource[counter][1]);
			counter++;
		}
	glEnd();
	//radiator
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <9; i++){
			glVertex2i(radiatorSource[counter][0], radiatorSource[counter][1]);
			counter++;
		}
	glEnd();
	//radiator
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <7; i++){
			glVertex2i(radiatorSource[counter][0], radiatorSource[counter][1]);
			counter++;
		}
	glEnd();
	//screen
	glColor4f(0.0,0.2,0.3,carAlpha);
	counter = 0;
	glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i <11; i++){
			glVertex2i(screenSource[counter][0], screenSource[counter][1]);
			counter++;
		}
	glEnd();
	counter = 0;
	//door
	glColor4f(0.0,0.2,0.3,carAlpha);
	glBegin(GL_POLYGON);
		for (int i = 0; i <5; i++){
			glVertex2i(doorSource[counter][0], doorSource[counter][1]);
			counter++;
		}
	glEnd();
	//door
	glColor4f(0.0,0.0,0.0,carAlpha);
	glBegin(GL_LINES);
		for (int i = 0; i <2; i++){
			glVertex2i(doorSource[counter][0], doorSource[counter][1]);
			counter++;
		}
	glEnd();
}
//draws wheels for cars
void wheels(){

	glColor4f(0.1,0.1,0.1,carAlpha);
	glBegin(GL_POLYGON); 
	for(int ii = 0; ii < 100; ii++) 
	{ 
		float rotate = 2.0f * 3.1415926f * float(ii) / float(100);//get the current angle 

		circleX = 58.5 * cosf(rotate);//calculate the x component 
		circleY = 58.5 * sinf(rotate);//calculate the y component 

		glVertex2f(circleX-25, circleY+5);//output vertex 

	}
	glEnd();

	glColor4f(0.0,0.0,0.0,carAlpha);
	glBegin(GL_POLYGON); 
	for(int ii = 0; ii < 100; ii++) 
	{ 
		float rotate = 2.0f * 3.1415926f * float(ii) / float(100);//get the current angle 

		circleX = 58.5 * cosf(rotate);//calculate the x component 
		circleY = 58.5 * sinf(rotate);//calculate the y component 

		glVertex2f(circleX, circleY);//output vertex 

	}
	glEnd();
	
	glColor4f(0.5,0.5,0.5,carAlpha);
	glBegin(GL_POLYGON); 
	for(int ii = 0; ii < 100; ii++) 
	{ 
		float rotate = 2.0f * 3.1415926f * float(ii) / float(100);//get the current angle 

		circleX = 35.5 * cosf(rotate);//calculate the x component 
		circleY = 35.5 * sinf(rotate);//calculate the y component 

		glVertex2f(circleX, circleY);//output vertex 

	}
	glEnd();
}
//draws background
void road(){
	glColor3f(0.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glVertex2i(-300,200);
		glVertex2i(800,200);
		glColor3f(0.3,0.3,0.3);
		glVertex2i(800,-100);
		glColor3f(0.2,0.2,0.2);
		glVertex2i(-300,-100);
	glEnd();
}
//calls draw function for engine cases
void drawEngine(){
	glPushMatrix();
		glTranslatef(0,0,0.0);
		glScalef((sX * engScale),(sY * engScale),0.0);
			engineLeft();
			engineRight();
	glPopMatrix();
}
//calls draw functions for car points and wheel points
void drawCar(){
	glPushMatrix();
		car();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(wheelsSource[0][0],wheelsSource[0][1],0.0);
	glScalef(wheelsScale[0],wheelsScale[1],1.0);
			wheels();
			glTranslatef(wheelsSource[1][0],wheelsSource[1][1],0.0);
	glScalef(wheelsScale[0],wheelsScale[1],0.9);
			wheels();
	glPopMatrix();
}
//calls function to draw background
void drawRoad(){
	glPushMatrix();
		road();
	glPopMatrix();
}
//calls draw functions for BMW logo
//
//uses the letter M to draw the letter W by flipping its Y axis
void drawBMW(){
	glPushMatrix();
		glTranslatef(-20,210,0.0);
		glRotatef(-35,0,0,1.0);
		glScalef(0.8,1,1.0);
		letterB();//function only draws 1 half
		glTranslatef(50,0,0.0);
		letterB();//draws second half to create full B
	glPopMatrix();
	glPushMatrix();
		glTranslatef(70,200,0.0);
		letterM();//function only draws 1 half
		glTranslatef(79,0,0.0);
		glScalef(-1,1,1.0);
		letterM();//draws second half to create full M
	glPopMatrix();
	glPushMatrix();
		glTranslatef(220,240,0.0);
		glRotatef(45,0,0,-1.0);
		glScalef(1,-1,1.0);
			letterM();//draws and flips 1st half of M
		glTranslatef(79,0,0.0);
		glScalef(-1,1,1.0);
			letterM();//draws and flips 2nd half of M
	glPopMatrix();
}
//calls draw function to draw BMW Badge
void drawBadge(){
	glPushMatrix();
		glTranslatef(120,80,0.0);	
			badge(1.0,1.0,1.0,90);//draws white badge
			quaterCircle(0.0,0.0,1.0,80);//draws quarter circle in blue
		glScalef(-1,-1,1.0);
			quaterCircle(0.0,0.0,1.0,80);//draws quarter circle in blue
	glPopMatrix();
}
//draws logo to screen
void slogan(){
	glPushMatrix();
		glRasterPos3f(-300,150,0);
		for(char *c = title; *c != '\0'; c++){
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
		}
	glPopMatrix();
}
//initializes window co-ordinates
void init (void)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-300.0,700.0,-100.0,800.0);
}


//tweens co-ordinates
//returns pointer to array
float* tween(int sourceX, int sourceY, int destX, int destY, float proportion){
	
	double differenceX = (sourceX - destX);
	double differenceY = (sourceY - destY);

	float * point;
	float temp [2] = {0.0,0.0};
	point = temp;

	temp [0] = (int)sourceX - (differenceX * proportion);
	temp [1] = (int)sourceY - (differenceY * proportion);

	return point;
}
//tweens co-ordinates
//recieves pointer arrays and calls function tween using array elements
//returns pointer
float* tweenCars(int (*p)[2],int (*p2)[2],int counter, float prop){
	
	float* point;
	point = tween(p[counter][0],p[counter][1],p2[counter][0],p2[counter][1],prop);
	return point;
}
//swaps pointer array elements (cars)
void swapCars(){
	int arr1 =0;
	int arr2 =1;
	for (int i = 0; i < 7; i++){
		int (*temp)[2];
		temp = pointerToCars[arr1];
		pointerToCars[arr1] = pointerToCars[arr2];
		pointerToCars[arr2] = temp;
		arr1 += 2;
		arr2 += 2;
	}
}
//swaps colour array elements
void swapColours(){
	float temp[3] = {carColour[0][0],carColour[0][1],carColour[0][2]};
	for (int i = 0; i < 3; i++){
		carColour[0][i] = carColour[1][i];
	}
	for (int i = 0; i < 3; i++){
		carColour[1][i] = temp[i];
	}
}

void display()
{

	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glClearColor (0,0,0,0.0);
	
	//first scene includes pistons, crankshaft, valves and engine
	//if (frame >= 4000){
	glViewport(0,0,1400,1000);
		drawRoad();
	//}
	if (frame <= 2550){
		drawValves();

		//viewport for pistons and crankshaft
		glViewport(sceneoneXY[0], sceneoneXY[1], 1000,1000);

		if (frame >= 1000){
			// slides pistons to the left
			tweenReturn = tween (sceneoneXY[0], sceneoneXY[1], -100, 0, proportion);
			sceneoneXY[0] = tweenReturn[0];
			sceneoneXY[1] = tweenReturn[1];
			if (frame < 2500){
				proportion += 0.001;
			}

			if (proportion > 1.0){
				proportion = 0.00;
			}
		}

		//fades in piston number 4
		if (frame >= 2000){
			if (alpha4 < 1.0) alpha4 += 0.001;
			else {
				alpha4 = 1.0;
			}
		}

		// slides valves into place
		if (frame >= 2000 && frame <= 2500){
			for (int count = 0; count < 8; count++){
				tweenReturn = tween (valveSource[count][0], valveSource[count][1], valveDest[count][0], valveDest[count][1], proportion);
				valveSource[count][0] = tweenReturn[0];
				valveSource[count][1] = tweenReturn[1];
			}
			proportion += 0.001;
			if (proportion > 1.0){
				proportion = 0.00;
			}
		}

		//sets valves to fixed final destination
		if (frame >2500){
			for (int count = 0; count < 8; count++){
				valveSource [count][0] = valveDest[count][0];
				valveSource [count][1] = valveDest[count][1];
			}
		}
		
		//fades in piston number 3
		if (frame >= 1500){
			if (alpha3 < 1.0) alpha3 += 0.001;
			else {
				alpha3 = 1.0;
			}
		}

		//fades in piston number 2
		if (frame >= 1000){
			if (alpha2 < 1.0) alpha2 += 0.001;
			else {
				alpha2 = 1.0;
			}
		}

		//Draw scene one. scene one includes pistons and crankshaft
		sceneOne();
	}

	//slides right side engine case in to view from the right
	if (frame == 2500) proportion = 0.00;
	if (frame >= 2500 && frame < 3250){
		for (int count = 0; count < 22; count++){
			tweenReturn = tween (engineRightSource[count][0], engineRightSource[count][1], engineRightDest[count][0], engineRightDest[count][1], proportion);
			engineRightSource[count][0] = tweenReturn[0];
			engineRightSource[count][1] = tweenReturn[1];
		}

		//slides left side engine case into view from the left
		for (int count = 0; count < 8; count++){
			tweenReturn = tween (engineLeftSource[count][0], engineLeftSource[count][1], engineLeftDest[count][0], engineLeftDest[count][1], proportion);
			engineLeftSource[count][0] = tweenReturn[0];
			engineLeftSource[count][1] = tweenReturn[1];
		}
		proportion += 0.01;

		if (proportion >= 1.0){
			proportion = 0.00;
		}

		//scales engine to begin scene 2
		if (frame >= 2550 && frame < 3350){
			engScale -= 0.001;
			engX += 0.6;
		}

		//viewport for engine
		glViewport(engX,100,2000,1000);
		drawEngine();
	}

	
	if(frame >=3100){
	//transition between scene 1 and 2
	//fades out engine - fades in car
		if (carAlpha < 1.0) {
			carAlpha += 0.01;
			engineAlpha -= 0.01;
		}
			else {
				carAlpha = 1.0;
				engineAlpha = 0.0;
			}

		if (frame == 3350) proportion = 0.00;
		//tweens car co-ordinates to next car co-ordinates
		if (frame >= 3350 && !pause){

			for (int count = 0; count < 46; count++){				
				tweenReturn = tweenCars (pointerToCars[0], pointerToCars[1],count, (proportion+0.2));
				carSource[count][0] = tweenReturn[0];
				carSource[count][1] = tweenReturn[1];
			}

			for (int count = 0; count < 16; count++){
				tweenReturn = tweenCars (pointerToCars[2], pointerToCars[3],count, (proportion+0.2));
				headLightSource[count][0] = tweenReturn[0];
				headLightSource[count][1] = tweenReturn[1];
			}
			for (int count = 0; count < 16; count++){
				tweenReturn = tweenCars (pointerToCars[4],pointerToCars[5],count, (proportion+0.2));
				grillSource[count][0] = tweenReturn[0];
				grillSource[count][1] = tweenReturn[1];
			}
			for (int count = 0; count < 21; count++){
				tweenReturn = tweenCars (pointerToCars[6],pointerToCars[7],count, (proportion+0.2));
				radiatorSource[count][0] = tweenReturn[0];
				radiatorSource[count][1] = tweenReturn[1];
			}
			for (int count = 0; count < 11; count++){
				tweenReturn = tweenCars (pointerToCars[8],pointerToCars[9],count, (proportion+0.2));
				screenSource[count][0] = tweenReturn[0];
				screenSource[count][1] = tweenReturn[1];
			}
			for (int count = 0; count < 7; count++){
				tweenReturn = tweenCars (pointerToCars[10],pointerToCars[11],count, (proportion+0.2));
				doorSource[count][0] = tweenReturn[0];
				doorSource[count][1] = tweenReturn[1];
			}
			for (int count = 0; count < 2; count++){
				tweenReturn = tweenCars (pointerToCars[12],pointerToCars[13],count, (proportion+0.2));
				wheelsSource[count][0] = tweenReturn[0];
				wheelsSource[count][1] = tweenReturn[1];
				//tweens wheels scale value for each car
				if (!car2View && wheelsScale[count] < 1.0){wheelsScale[count] += 0.01;}
				else if (car2View && wheelsScale[0] > 0.5){wheelsScale[0] -= 0.01;}
				else if (car2View && wheelsScale[1] > 0.8){wheelsScale[1] -= 0.01;}
			}
			//changes car colour
			if (red < carColour[1][0]){red += 0.01;}
			else {red -= 0.01;}

			if (green > carColour[1][1]){green -= 0.01;}
			else {green += 0.01;}

			if (blue > carColour[1][2]){blue -= 0.01;}
			else {blue += 0.01;}

			proportion += 0.01;
		}
		//swaps pointer arrays to switch between cars
		//pauses the car morphs
		//fixes drawn car to final co-ordinates
		if (proportion > 0.9 && frame >= 3350){
			swapCars();
			swapColours();
			proportion = 0.0;
			pause = true;
			for (int count = 0; count < 46; count++){
				carSource[count][0] = pointerToCars[0][count][0];
				carSource[count][1] = pointerToCars[0][count][1];
			}
			for (int count = 0; count < 16; count++){
				headLightSource[count][0] = pointerToCars[2][count][0];
				headLightSource[count][1] = pointerToCars[2][count][1];
			}
			for (int count = 0; count < 16; count++){
				grillSource[count][0] = pointerToCars[4][count][0];
				grillSource[count][1] = pointerToCars[4][count][1];
			}
			for (int count = 0; count < 21; count++){
				radiatorSource[count][0] = pointerToCars[6][count][0];
				radiatorSource[count][1] = pointerToCars[6][count][1];
			}
			for (int count = 0; count < 11; count++){
				screenSource[count][0] = pointerToCars[8][count][0];
				screenSource[count][1] = pointerToCars[8][count][1];
			}
			for (int count = 0; count < 7; count++){
				doorSource[count][0] = pointerToCars[10][count][0];
				doorSource[count][1] = pointerToCars[10][count][1];
			}
			for (int count = 0; count < 2; count++){
				wheelsSource[count][0] = pointerToCars[12][count][0];
				wheelsSource[count][1] = pointerToCars[12][count][1];
			}
		}
		//draws car
		glViewport(100,30,1300,1200);
		drawCar();
	}

	if (frame > 4000){
		//draws slogan
		slogan();
	}
	//draws BMW badge
	if (frame > 5000){
		glViewport(-50,380,600,600);
		logoAlpha += 0.1;
		drawBadge();
		drawBMW();
	}
	//swaps betweens buffers
	glutSwapBuffers();

	glFlush();
}

void Timer(int value)
{
	if (frame == 0){
		pointerToCurrentCar = car1;
		pointerToNextCar = car2;
		pointerToCurrentHeadlight = headlightpoints;
		pointerToNextHeadlight = headlightpoints2;
		pointerToCurrentGrill = grillpoints;
		pointerToNextGrill = grillpoints2;
		pointerToCurrentRadiator = radiatorpoints;
		pointerToNextRadiator = radiatorPoints2;
		pointerToCurrentScreen = windscreenPoints;
		pointerToNextScreen = windscreenPoints2;
		pointerToCurrentDoor = doorPoints;
		pointerToNextDoor = doorPoints2;
		pointerToCurrentWheels = wheelsPos;
		pointerToNextWheels = wheelsPos2;

		pointerToCars[0] = pointerToCurrentCar;
		pointerToCars[1] = pointerToNextCar;
		pointerToCars[2] = pointerToCurrentHeadlight;
		pointerToCars[3] = pointerToNextHeadlight;
		pointerToCars[4] = pointerToCurrentGrill;
		pointerToCars[5] = pointerToNextGrill;
		pointerToCars[6] = pointerToCurrentRadiator;
		pointerToCars[7] = pointerToNextRadiator;
		pointerToCars[8] = pointerToCurrentScreen;
		pointerToCars[9] = pointerToNextScreen;
		pointerToCars[10] = pointerToCurrentDoor;
		pointerToCars[11] = pointerToNextDoor;
		pointerToCars[12] = pointerToCurrentWheels;
		pointerToCars[13] = pointerToNextWheels;
		for (int i = 0; i<46; i++){
			carSource[i][0] = car1[i][0];
			carSource[i][1] = car1[i][1];
		}

		for (int count = 0; count < 16; count++){
			headLightSource[count][0] = headlightpoints[count][0];
			headLightSource[count][1] = headlightpoints[count][1];
		}
		for (int count = 0; count < 16; count++){
			grillSource[count][0] = grillpoints[count][0];
			grillSource[count][1] = grillpoints[count][1];
		}
		for (int count = 0; count < 21; count++){
			radiatorSource[count][0] = radiatorpoints[count][0];
			radiatorSource[count][1] =radiatorpoints[count][1];
		}
		for (int count = 0; count < 11; count++){
			screenSource[count][0] = windscreenPoints[count][0];
			screenSource[count][1] = windscreenPoints[count][1];
		}
		for (int count = 0; count < 7; count++){
			doorSource[count][0] = doorPoints[count][0];
			doorSource[count][1] = doorPoints[count][1];
		}
		for (int count = 0; count < 2; count++){
			wheelsSource[count][0] = wheelsPos[count][0];
			wheelsSource[count][1] = wheelsPos[count][1];
		}
	}
	//increases frame
	frame++;
	
	//Rotates camshaft and base of ConnectionRod for SceneOne and translates piston up and down
	theta = theta-2;
	if (value == 0){
		pistonY -= 1;
		pistonY2 += 1;
	}
	else {
		pistonY += 1;
		pistonY2 -= 1;
	}

	if (value == 0 && pistonY >= -45)
		conRodX++;
	else if (pistonY <= -45)
		conRodX--;
	else
		conRodX++;

	glutPostRedisplay();

	if (pistonY <= -90)
		value = 1;
	else if (pistonY >=0)
		value = 0;

	if (pause){
		morphCounter++;
	}
	if (morphCounter >= 700){
		pause = false;
		if(car2View){car2View = false;}
		else {car2View = true;}
		morphCounter = 0;
	}

	glutTimerFunc(10, Timer, value);
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(1000,600);
	glutInitWindowPosition(100,100);
	glutCreateWindow("A Bit Of Drawing");
	init();
	glutDisplayFunc(display);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Timer(0);
	
	glutMainLoop();
	return 0;
	exit(0);
}

